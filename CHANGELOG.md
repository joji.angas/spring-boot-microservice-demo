<h1>CHANGELOG</h1>

This changelog was retroactively created<br>
based on the descriptions from the comments on the commit points:<br>
Starting from the first commit, up to the most recent,<br>
as of this file's creation: (c8ce32c0f7fe362c1fbcdd2c419f965f67e1189a)

<h3>Nov 7, 2020 - Initial Commit</h3>

* Creation of GitLab project repository. ***Master*** branch was the initial branch available for commits.

<h3>Nov 8, 2020 - Creation of Application Scaffold</h3>

* The base application directory and package structure was created using Spring Initializr (https://start.spring.io/)
* Base classes, pom file, and other artefacts were generated based on the settings that were
selected from the site. These artefacts, along with the base application directory structure, were placed on a downloadable zip file, available for download.
* This is the initial commit of these artefacts to the repository after generation, download, and extraction.
* Initial components configured in POM:
  * Spring Boot Database + JPA
  * Spring Boot Bean Validation
  * Spring Boot Web
  * H2 Database (initial in-memory database)
  * Lombok
  * JUnit 5
* ***Latest*** branch created to serve as active base branch for checking out and making updates to code, logic, and configuration. 

<h3>Oct 6, 2021 - Milestone 1: Functionality Implemented After a Long Hiatus</h3>

* Swagger dependencies were added to the pom. Swagger configuration class created, and initial configuration was started.<br>
* Controller, Service and Database Model classes were created.
* Initial functionalities for these classes and interfaces were defined.
* Addition of dependencies related to Bean Validation were added.<br>
Constraints were defined onto fields. And validation messages were configured into these fields.
* Generic exception handler class was created. Added support for handling validation exceptions.
Exceptions handled include:
  * Http Server Errors (Error 500)
  * Validation Errors (Error 400)
  * Other internal errors (Error 500)
* Database functions now working and can be triggered from the Controller endpoints.<br>
Records and their values are stored in H2 in-memory database.
* ***0.0.1-ALPHA*** branch created to serve as active base branch for current iteration of changes.

<h3>Oct 16, 2021 - Introduction of Serialization / Deserialization Functions For Some Fields</h3>

* Account Status field display and data storage values have been formatted through serialization and deserialization functions.
* Account Status field stores numeric value on the database, but when displayed on the response, shows the equivalent String values (**ACTIVE**, **INACTIVE**, **DISABLED**, or **PURGED**). Please see [AccountStatus.java](/src/main/java/net/garapata/demo/dto/enums/AccountStatus.java) for more details.

<h3>May 15, 2022 - End of Milestone 1: Table Fields Cleanup and Updates, Adding of reference documents, Transition to Next Phase</h3>

* Performed cleanup and renaming of fields from the ***Login*** table.
* Merged all working changes from ***latest*** to ***master*** branch.
* Created branch **0.0.1-ALPHA** (Oct 6, 2021) from ***latest*** branch to mark all the features that have been successfully implemented so far. Features include:
  * **Swagger UI configuration** / service endpoints UI
  * **Service, Controller, Repository, Model, and Exception-handling** classes have been created. Intended functions have been implemented.
  * **Fields and Parameter Validation** had been implemented.
  * **Serialization and Deserialization** of field values have been implemented.
  * **CRUD^** functions for database have been implemented end to end starting from Service up to the Controller level.
    * **^CREATE, READ, UPDATE, and DELETE** functions from the database are accessible through the Service and Controller methods.
    * Specialized versions of these functions, such as **Change / Reset Password, Change / Update Email, Find by Username, and Find by Email Address** have been implemented. Only these methods have been exposed, along with **Create Account, and Delete Account** as they are the only functionality that are needed, for now. 
  * **Specialized Service and Controller-level methods** are able to successfully access and manipulate data on the database as intended.

<h3>May 16, 2022 - Milestone 2: Start of Next Phase: New Database, New Deployment Strategy, Upgrading The Application Stack Implementation</h3>

* Changed database used to from **H2** to **MySQL**.
* Created a db initialization script for use in testing.
* Created a shell script for automating the execution of the db initialization script. Status: *In-Progress*
* Transitioning to a containerized deployment model. Already started with the MySQL database. Connecting the local deployed application to a containerized instance of the database, and was successful. 
* Created a Docker Compose yaml configuration file for orchestrated application stack build. Configuration has not been started yet.
* Updated properties file with MySQL details and credentials.

<h3>Jun 8, 2022 - Creation of CHANGELOG File, Documentation Files Updates</h3>

* Created this CHANGELOG file, and updated README and FEATURES documentation files.