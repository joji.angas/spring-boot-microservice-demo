-- login database creation

DROP DATABASE IF EXISTS `login`;

CREATE DATABASE `login` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- login table creation

DROP TABLE IF EXISTS login.login;

-- login.login definition

CREATE TABLE login.login (
	login_id BIGINT auto_increment NOT NULL,
	username varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
	password varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	email varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	account_status INT DEFAULT 0 NOT NULL,
	create_timestamp DATETIME NULL,
	update_timestamp DATETIME NULL,
	CONSTRAINT login_PK PRIMARY KEY (login_id),
	CONSTRAINT login_UN UNIQUE KEY (username)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;
CREATE INDEX login_login_id_IDX USING BTREE ON login.login (login_id);


