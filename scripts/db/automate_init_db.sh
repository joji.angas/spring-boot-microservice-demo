#!/usr/bin/bash

# init vars
db_instance="gt-mysql8"


init_db()
{
   sudo docker run --name $db_instance -e MYSQL_ROOT_PASSWORD=my-secret-pw -p 3306:3306/tcp -d mysql:8
   sleep 5
   sudo docker container ls -a
   do_init 
}

do_init()
{
   sudo docker exec -i $db_instance sh -c 'exec mysql -uroot -p"$MYSQL_ROOT_PASSWORD"' < initialize_database.sql
}


cleanup_db()
{
   sudo docker container stop $db_instance
   sudo docker container prune -f
}

# main
# run from docker server, to start mysql docker container
init_db
echo -e "end"
