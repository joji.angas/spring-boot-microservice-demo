package net.garapata.demo.db;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.extern.slf4j.Slf4j;
import net.garapata.demo.model.LoginAccount;
import net.garapata.demo.repository.LoginAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Slf4j
public class LoginAccountTest {

    @MockBean
    private LoginAccountRepository loginAccountRepository;

    private ObjectMapper objectMapper;
    private LoginAccount loginAccount;

    @BeforeEach
    public void init(){
        objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        loginAccount = LoginAccount.builder()
                .username("jangas")
                .password("Sh0ryuk3n5!")
                .email("joji.angas@gmail.com")
                .build();
    }

    @Test
    public void testLoginAccountConstraints() throws JsonProcessingException {
        log.info("Login Account: \n {}", objectMapper.writeValueAsString(loginAccount));
        assertNotNull(loginAccount);
    }

    @Test
    public void testLoginAccountRepository() throws JsonProcessingException {
        when(loginAccountRepository.save(loginAccount)).thenReturn(loginAccount);
        when(loginAccountRepository.findByUsername("jangas")).thenReturn(Optional.of(loginAccount));
        Optional<LoginAccount> retrievedAccount = loginAccountRepository.findByUsername("jangas");
        log.info("Login Account: \n {}", objectMapper.writeValueAsString(retrievedAccount.get()));
        assertEquals(loginAccount, retrievedAccount.get());
    }
}
