package net.garapata.demo.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import net.garapata.demo.model.LoginAccount;

import java.util.List;

public interface LoginAccountService {
    LoginAccount addNewAccount(LoginAccount newLoginAccount) throws JsonProcessingException;
    LoginAccount findByUserName(String username);
    List<LoginAccount> findByEmail(String email);
    LoginAccount changePassword(String password, Long id);
    LoginAccount changeEmail(String email, Long id);
    Boolean deleteAccount(long id);
}
