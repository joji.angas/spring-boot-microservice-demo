package net.garapata.demo.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import net.garapata.demo.dto.enums.AccountStatus;
import net.garapata.demo.model.LoginAccount;
import net.garapata.demo.repository.LoginAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@Slf4j
public class LoginAccountServiceImpl implements LoginAccountService {
    @Autowired
    LoginAccountRepository loginAccountRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Override
    public LoginAccount addNewAccount(LoginAccount newLoginAccount) throws JsonProcessingException {
        log.info("Creating new login account: \n {}", objectMapper.writeValueAsString(newLoginAccount));
        newLoginAccount.setCreateTimestamp(LocalDateTime.now());
        newLoginAccount.setAccountStatus(AccountStatus.ACTIVE.ordinal());
        return loginAccountRepository.save(newLoginAccount);
    }

    @Override
    public LoginAccount findByUserName(String username) {
        LoginAccount loginResult = new LoginAccount();
        log.info("Finding login account with username " + username);
        Optional<LoginAccount> loginResults = loginAccountRepository.findByUsername(username);
        if(loginResults.isPresent()) {
            loginResult = loginResults.get();
        } else { throw new HttpServerErrorException(NOT_FOUND); }
        return loginResult;
    }

    @Override
    public List<LoginAccount> findByEmail(String email) {
        List<LoginAccount> loginAccounts = null;
        log.info("Finding login account with email " + email);
        Optional<List<LoginAccount>> repositoryResults = loginAccountRepository.findByEmail(email);
        if(repositoryResults.isPresent()) {
            log.info("results is present");
            loginAccounts = repositoryResults.get();
        } else { throw new HttpServerErrorException(NOT_FOUND); }
        return loginAccounts;
    }

    @Override
    public LoginAccount changePassword(String password, Long id) {
        Optional<LoginAccount> account = loginAccountRepository.findById(id);
        LoginAccount updateAccount = null;
        if(account.isPresent()){
            updateAccount = account.get();
            updateAccount.setPassword(password);
            updateAccount.setUpdateTimestamp(LocalDateTime.now());
            updateAccount = loginAccountRepository.save(updateAccount);
        } else { throw new HttpServerErrorException(NOT_FOUND); }
        return updateAccount;
    }

    @Override
    public LoginAccount changeEmail(String email, Long id) {
        Optional<LoginAccount> account = loginAccountRepository.findById(id);
        LoginAccount updateAccount = null;
        if(account.isPresent()){
            updateAccount = account.get();
            updateAccount.setEmail(email);
            updateAccount.setUpdateTimestamp(LocalDateTime.now());
            updateAccount = loginAccountRepository.save(updateAccount);
        } else { throw new HttpServerErrorException(NOT_FOUND); }
        return updateAccount;
    }

    @Override
    public Boolean deleteAccount(long id) {
        try{
            loginAccountRepository.deleteById(id);
            return true;
        } catch (Exception ex){
            return false;
        }
    }
}
