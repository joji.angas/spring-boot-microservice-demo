package net.garapata.demo.dto.viewmodel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonRootName("response")
public class ViewResponse<T> {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T body;
    private String status;
    private String statusCode;
    private String description;

    public ViewResponse(T t) {
        this.body = t;
    }
}
