package net.garapata.demo.dto.enums;

import lombok.Getter;

@Getter
public enum AccountStatus {
    ACTIVE, INACTIVE, DISABLED, PURGED;
    public static String getStatusString(int index){
        String statusName = null;
        for(AccountStatus status: AccountStatus.values()){
            if(status.ordinal() == index) statusName = status.name();
        }
        return statusName;
    }
}
