package net.garapata.demo.repository;

import net.garapata.demo.model.LoginAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LoginAccountRepository extends JpaRepository<LoginAccount, Long> {
    Optional<List<LoginAccount>> findByEmail(String email);
    Optional<LoginAccount> findByUsername(String username);
}
