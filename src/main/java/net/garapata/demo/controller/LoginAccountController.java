package net.garapata.demo.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.garapata.demo.dto.viewmodel.ViewResponse;
import net.garapata.demo.model.LoginAccount;
import net.garapata.demo.service.LoginAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/login")
@Api(value = "LoginAccount")
public class LoginAccountController {

    @Autowired
    LoginAccountService loginAccountService;

    @ApiOperation(value = "Add new Account")
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ViewResponse<LoginAccount> addNewAccount(@RequestBody @Valid LoginAccount login) throws HttpServerErrorException.InternalServerError {
        try {
            return generateViewResponse(new ViewResponse<LoginAccount>(loginAccountService.addNewAccount(login)));
        } catch (JsonProcessingException e) {
            throw new HttpServerErrorException(INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Find Account by username")
    @GetMapping(value = "/find/{username}", produces = MediaType.APPLICATION_JSON_VALUE, params = "username")
    public ViewResponse<LoginAccount> getAccountByUsername(@RequestParam(name = "username") String username){
        return generateViewResponse(new ViewResponse<LoginAccount>(loginAccountService.findByUserName(username)));
    }

    @ApiOperation(value = "Find Account by e-mail address")
    @GetMapping(value = "/find/{email}", produces = MediaType.APPLICATION_JSON_VALUE, params = "email")
    public ViewResponse<List<LoginAccount>> getAccountByEmail(@RequestParam(name = "email") String email){
        return generateViewResponse(new ViewResponse<List<LoginAccount>>(loginAccountService.findByEmail(email)));
    }

    @ApiOperation(value = "Reset/change password for the Account")
    @PutMapping(value = "/resetPassword/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, params = "id")
    public ViewResponse<LoginAccount> resetPassword(@RequestBody @NotNull String password, @RequestParam @NotEmpty long id){
        ViewResponse<LoginAccount> resetResponse = new ViewResponse<LoginAccount>();
        resetResponse.setBody(loginAccountService.changePassword(password, id));
        resetResponse.setStatus("Reset Password Success");
        resetResponse.setStatusCode(OK.toString());
        resetResponse.setDescription("Password has been reset.");
        return resetResponse;
    }

    @ApiOperation(value = "Reset/change e-mail address for the Account")
    @PutMapping(value = "/updateEmail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, params = "id")
    public ViewResponse<LoginAccount> updateEmail(@RequestBody @Email String email, @RequestParam @NotEmpty long id){
        ViewResponse<LoginAccount> resetResponse = new ViewResponse<LoginAccount>();
        resetResponse.setBody(loginAccountService.changeEmail(email, id));
        resetResponse.setStatus("Update Email Success");
        resetResponse.setStatusCode(OK.toString());
        resetResponse.setDescription("Email has been updated.");
        return resetResponse;
    }

    @ApiOperation(value = "Delete selected Account")
    @GetMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE, params = "id")
    public ViewResponse deleteAccount(@RequestParam(name = "id") @NotNull long id){
        ViewResponse deleteResponse = new ViewResponse();
        if(loginAccountService.deleteAccount(id)) {
            deleteResponse.setDescription("Account has been deleted.");
            deleteResponse.setStatus("Delete Account Success");
            deleteResponse.setStatusCode(OK.toString());
        }
        return deleteResponse;
    }

    private ViewResponse generateViewResponse(ViewResponse response){
        response.setStatusCode(OK.toString());
        response.setStatus("Success");
        response.setDescription("Request Successful");
        return response;
    }
}
