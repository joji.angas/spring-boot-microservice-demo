package net.garapata.demo.config.exception;

import net.garapata.demo.dto.viewmodel.ViewResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException metArgNVEx, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, String> validationErrors = new HashMap<String, String>();
        metArgNVEx.getBindingResult().getAllErrors().forEach((error)->{
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            validationErrors.put(fieldName, errorMessage);
        });
        ViewResponse errorResponse = new ViewResponse();
        errorResponse.setBody(validationErrors);
        errorResponse.setStatus("Validation Error");
        errorResponse.setDescription("Invalid field values were entered.");
        errorResponse.setStatusCode(HttpStatus.BAD_REQUEST.toString());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpServerErrorException.class)
    public ResponseEntity<ViewResponse> handleInternalServerErrors(HttpServerErrorException isEx){
        ViewResponse errorResponse = new ViewResponse();
        errorResponse.setDescription(isEx.getLocalizedMessage());
        errorResponse.setStatus(isEx.getStatusText());
        errorResponse.setStatusCode(isEx.getStatusCode().toString());
        return new ResponseEntity<>(errorResponse, isEx.getStatusCode());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ViewResponse> handleOtherExceptions(Exception ex){
        ViewResponse errorResponse = new ViewResponse();
        errorResponse.setStatus("Error");
        errorResponse.setDescription(ex.getLocalizedMessage());
        errorResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
