package net.garapata.demo.util.serialization;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import net.garapata.demo.dto.enums.AccountStatus;

import java.io.IOException;

public class AccountStatusSerializer extends StdSerializer<Integer> {

    public AccountStatusSerializer() { this(null); }

    protected AccountStatusSerializer(Class<Integer> t) {
        super(t);
    }

    @Override
    public void serialize(Integer accountStatus, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(AccountStatus.getStatusString(accountStatus));
    }
}
