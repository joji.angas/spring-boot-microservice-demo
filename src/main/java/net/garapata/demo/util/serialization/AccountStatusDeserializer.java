package net.garapata.demo.util.serialization;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import net.garapata.demo.dto.enums.AccountStatus;

import java.io.IOException;

public class AccountStatusDeserializer extends StdDeserializer<Integer> {

    public AccountStatusDeserializer() { this(null); }

    protected AccountStatusDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Integer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        return AccountStatus.valueOf(jsonParser.getText()).ordinal();
    }
}
