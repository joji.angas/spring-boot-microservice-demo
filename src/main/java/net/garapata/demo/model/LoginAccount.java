package net.garapata.demo.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import net.garapata.demo.util.serialization.AccountStatusDeserializer;
import net.garapata.demo.util.serialization.AccountStatusSerializer;
import net.garapata.demo.util.serialization.TimeStampDeserializer;
import net.garapata.demo.util.serialization.TimeStampSerializer;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

import static net.garapata.demo.dto.enums.AccountStatus.ACTIVE;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "login")
public class LoginAccount {
    @Id
    @Column(name = "login_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long loginId;
    @ApiModelProperty(notes = "The account username")
    @Column(nullable = false, unique = true)
    @NotBlank(message = "Username cannot be blank")
    private String username;
    @ApiModelProperty(notes = "The account password")
    @Column(nullable = false)
    @NotBlank(message = "Password cannot be blank")
    private String password;
    @Column(nullable = false)
    @NotBlank(message = "Email cannot be blank")
    @Email(message = "Invalid email format")
    private String email;
    @Column(name = "account_status", nullable = false, length = 1)
    @JsonDeserialize(using = AccountStatusDeserializer.class)
    @JsonSerialize(using = AccountStatusSerializer.class)
    private int accountStatus;
    @Column(name = "create_timestamp")
    @JsonSerialize(using = TimeStampSerializer.class)
    @JsonDeserialize(using = TimeStampDeserializer.class)
    private LocalDateTime createTimestamp;
    @Column(name = "update_timestamp")
    @JsonSerialize(using = TimeStampSerializer.class)
    @JsonDeserialize(using = TimeStampDeserializer.class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LocalDateTime updateTimestamp;

    @Builder
    public LoginAccount(String username, String password, String email){
        this.username = username;
        this.password = password;
        this.email = email;
        this.accountStatus = ACTIVE.ordinal();
        this.createTimestamp = LocalDateTime.now();
    }
}
