<h1>The <span style="color:#00DDFF">spring-boot-microservice-demo</span> Application</h1> 

This is a demo Spring Boot application which was modelled based on the ***microservices*** architecture. <br><br>
This approach highlights the usage of the Spring / Spring Boot Repository, Service, and Controller <br>
components to organize the persistence, business logic, and the web request / response routing for the application. <br><br>
The approach to creating the application is to implement it as a full-fledged microservice application,<br>
thereby exposing the application's strengths and weaknesses in both design and implementation,<br>
and start making improvements from there.

To check out the list of components, libraries, APIs, frameworks, and other technologies that have been used in this application, please visit the [ FEATURES ](/FEATURES.md) page.

For a chronological list of the changes and improvements in the application's business logic, configuration, and deployment strategy, please see the [ CHANGELOG. ](/CHANGELOG.md)