<h1>Application Features</h1>

<h2>Technology:</h2>

<h4>Spring / Spring Boot Components Used:</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Database + JPA<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spring (Bean) Validation<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Web<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Testing (with JUnit5)<br>

<h4>Database/s Used:</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H2 (v1)<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MySQL (v2)<br>

<h4>Other Libraries / APIs Used:</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lombok<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swagger 2 (Springfox)<br>

<h4>Deployment Configuration/s:</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker Images (from DockerHub) <br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker Compose <br>

&nbsp;&nbsp;&nbsp;<span style="color:#CC9966">**Coming Soon:**</span><br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amazon Elastic Compute Cloud (EC2)<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amazon Elastic Container Services (ECS)<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amazon Relational Database Service (RDS)<br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Amazon Elastic Container Service (ECS)<br>

<h2>Exposed Functions:</h2>
<h4>Service Endpoint (Base URL):</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://localhost:8580/login

<h4>Other Exposed Resources:</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H2 Database Console (If in use) - <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://localhost:8580/h2-console <br><br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swagger 2 Web UI <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(For endpoint URLs testing and access) - <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;http://localhost:8580/swagger-ui/ <br>

<h4>Serializers / Deserializers</h4>

* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Status Serializers / Deserializers <br>
* &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Timestamp Serializers / Deserializers <br>